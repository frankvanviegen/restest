const express = require('express');

const app = express();
app.use(express.json({limit: 65536}));

let heapMax;

let data = {};
let secretLru = [];

let counterSymbol = Symbol("counter");

function getCollection(req) {
	let {secret, collection} = req.params;
	let collections = data[secret];
	if (collections === undefined) {
		collections = data[secret] = {};
		secretLru.push(secret);
	}
	if (!collections.hasOwnProperty(collection)) {
		collections[collection] = {[counterSymbol]: 0};
	}
	return collections[collection];
}

function cleanData() {
	let index = 0;
	while (index < secretLru.length && heapMax && process.memoryUsage().heapUsed > heapMax) {
		delete data[secretLru[index]];
		index++;
	}
	secretLru.splice(0, index);
}

app.get('/', async function(req,rsp) {
	rsp.send({
		application: 'RESTest',
		documentation: 'For documentation please refer to: https://gitlab.com/frankvanviegen/restest',
	});
});

app.get('/:secret', async function(req,rsp) {
	let collections = data[req.params.secret] || {};
	rsp.send({collections: Object.keys(collections)});
});

app.post('/:secret/:collection', async function(req,rsp) {
	let collection = getCollection(req);
	if (req.body.id==null) {
		do {
			req.body.id = collection[counterSymbol] += 1;
		} while (collection.hasOwnProperty(req.body.id));
	} else {
		if (collection.hasOwnProperty(req.body.id)) {
			rsp.status(409).send({error: "Duplicate id"});
			return;
		}
	}
	collection[req.body.id] = req.body;
	
	rsp.status(201).send({id: req.body.id});
	cleanData();
});


app.get('/:secret/:collection', async function(req,rsp) {
	let collection = getCollection(req);
	rsp.send(Object.values(collection));
});


app.put('/:secret/:collection/:id', async function(req,rsp) {
	let collection = getCollection(req);
	if (!collection.hasOwnProperty(req.params.id)) {
		rsp.status(404).send({error: "No such item"});
		return;
	}
	let item = collection[req.params.id];

	if (req.body.hasOwnProperty('id') && req.body.id != item.id) {
		rsp.status(400).send({error: "Cannot change id"});
		return;
	}
	Object.assign(item, req.body);
	
	rsp.send(item);
	cleanData();
});


app.get('/:secret/:collection/:id', async function(req,rsp) {
	let collection = getCollection(req);
	if (!collection.hasOwnProperty(req.params.id)) {
		rsp.status(404).send({error: "No such item"});
		return;
	}
	rsp.send(collection[req.params.id]);
});

app.delete('/:secret/:collection/:id', async function(req,rsp) {
	let collection = getCollection(req);
	if (!collection.hasOwnProperty(req.params.id)) {
		rsp.status(404).send({error: "No such item"});
		return;
	}
	delete collection[req.params.id];
	rsp.send({});
});

app.delete('/:secret/:collection', async function(req,rsp) {
	if (data.hasOwnProperty(req.params.secret)) {
		delete data[req.params.secret][req.params.collection];
	}
	rsp.send({});
});

app.delete('/:secret', async function(req,rsp) {
	delete data[req.params.secret];
	rsp.send({});
});

var listener = app.listen(process.env.PORT, function () {
	heapMax = process.memoryUsage().heapUsed + 64 * 1024 * 1024;
	console.log('Your app is listening on port ' + listener.address().port);
});

