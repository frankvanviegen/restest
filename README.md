## RESTest

This is a generic REST server for testing/prototyping purposes. It stores all the data you throw at it using POSTs and PUTs for (usually) a couple of hours, and gives it back when you GET it. There is no authentication.

The following types of requests are supported:

- **POST /{secret}/{collection}**

  Add an item to the named collection. The secret should be something hard to guess such that your tests don't interfere with anybody else's tests. The item data should be given as a JSON object in the post body. If the object does not contain an 'id' key, it will be auto generated. The request will return either:
  - Status code 201, with a JSON body containing just the 'id' key, in case of success.
  - Status code 409, with a JSON body containing {"error": "Duplicate id"}, in case the provided key already exists.

  -----

  - Example: `http POST http://yourserver.example.com/GfSvefFwQv/people first=Piet last=Puk`
  - Returns: `{"id": 1}`

- **GET /{secret}/{collection}**

  Returns the full data for all items in the collection, or an empty list if the collection doesn't exist yet.

  - Example: `http GET http://yourserver.example.com/GfSvefFwQv/people`
  - Returns: `[{"id": 1, "first": "Piet", "last": "Puk"}, {"id": 2, "brand": "Tesla", "model": "X"}]`

- **GET /{secret}/{collection}/{id}**

  Returns the full data for a specific item in the collection. Or, if the given id does not exist within the collection, returns status code 404 with body `{"error": "No such item"}`.

  - Example: `http GET http://yourserver.example.com/GfSvefFwQv/people/1`
  - Returns: `{"id": 1, "first": "Piet", "last": "Puk"}`
  
- **PUT /{secret}/{collection}/{id}**

  Updates the specific item in the collection with the data from the request body. Fields not given in the body will be left as they were. Returns the full merged data for the item. If the given id does not exist within the collection, returns status code 404 with body `{"error": "No such item"}`.

  - Example: `http PUT http://yourserver.example.com/GfSvefFwQv/people/1 last=Luck age=15`
  - Returns: `{"id": 1, "first": "Piet", "last": "Luck", "age": "15"}`
  
- **GET /{secret}**
  
  Returns an object containing a list of collections for this secret.
  
  - Example: `http PUT http://yourserver.example.com/GfSvefFwQv`
  - Returns: `{"collections": ["people"]}`

- **DELETE /{secret}/{collection}/{id}** and **DELETE /{secret}/{collection}** and **DELETE /{secret}**

  These do exactly what you would expect.

